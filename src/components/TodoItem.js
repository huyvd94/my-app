import React, { Component } from 'react';
import './TodoItem.css';
import check from '../img/check.svg';
import checkComplete from '../img/check-ok.svg';

class TodoItem extends Component {

    render() {
        const { item, onClick } = this.props;

        let className = 'TodoItem';
        if (item.isComplete) {
            className += ' TodoItem-Complete';
        }

        let url = check;
        if (item.isComplete) {
            url = checkComplete;
        }
        return (
            <div className={className}>
                <img onClick={onClick} src={url} alt=""></img>
                <p> {this.props.item.title} </p>
            </div>
        );
    }
}

export default TodoItem;
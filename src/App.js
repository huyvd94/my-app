import React, { Component } from 'react';
import './App.css';
import TodoItem from './components/TodoItem'

class App extends Component {
  constructor() {
    super();
    this.state = {
      todoItems: [
        { title: 'Ăn sáng', isComplete: true },
        { title: 'Caf Phee', isComplete: false },
        { title: 'Hoc reactJS', isComplete: true },
      ]
    }
  }

  onItemClicked(item) {
    return () => {
      const isComplete = item.isComplete;
      const { todoItems } = this.state;
      const index = this.state.todoItems.indexOf(item);

      this.setState({
        todoItems: [
          ...todoItems.slice(0, index),
          {
            ...item,
            isComplete: !isComplete
          },
          ...todoItems.slice(index + 1)
        ]
      })
    }
  }

  render() {
    let { todoItems: TodoItems } = this.state;
    if (TodoItems.length > 0) {
      return (
        <div className="App">
          {
            TodoItems.map((item, index) =>
              <TodoItem
                key={index}
                item={item}
                onClick={this.onItemClicked(item)} />
            )
          }
        </div>
      );
    } else {
      return <div className="App">noitem</div>
    }
  }
}

export default App;
